<?php

namespace App\Form;

use App\Entity\IdentityCard;
use App\Entity\BulletinTrois;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BulletinTroisType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('information')
            ->add('dateCreation')
            ->add('identityCard', EntityType::class, [
                // looks for choices from this entity
                'class' => IdentityCard::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'identityCardNumber',
            
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BulletinTrois::class,
        ]);
    }
}
