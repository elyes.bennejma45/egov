<?php

namespace App\Controller;

use App\Entity\IdentityCard;
use App\Form\IdentityCardType;
use App\Repository\IdentityCardRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;

#[Route('/identityCard')]
class IdentityCardController extends AbstractController
{

    #[Route('/', name: 'app_identity_card_index', methods: ['GET'])]
    public function index(IdentityCardRepository $identityCardRepository): Response
    {
        return $this->render('identity_card/index.html.twig', [
            'identity_cards' => $identityCardRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_identity_card_new', methods: ['GET', 'POST'])]
    public function new(Request $request, IdentityCardRepository $identityCardRepository): Response
    {
        $identityCard = new IdentityCard();
        $form = $this->createForm(IdentityCardType::class, $identityCard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $identityCardRepository->save($identityCard, true);

            return $this->redirectToRoute('app_identity_card_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('identity_card/new.html.twig', [
            'identity_card' => $identityCard,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_identity_card_show', methods: ['GET'])]
    public function show(IdentityCard $identityCard): Response
    {
        return $this->render('identity_card/show.html.twig', [
            'identity_card' => $identityCard,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_identity_card_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, IdentityCard $identityCard, IdentityCardRepository $identityCardRepository): Response
    {
        $form = $this->createForm(IdentityCardType::class, $identityCard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $identityCardRepository->save($identityCard, true);

            return $this->redirectToRoute('app_identity_card_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('identity_card/edit.html.twig', [
            'identity_card' => $identityCard,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_identity_card_delete', methods: ['POST'])]
    public function delete(Request $request, IdentityCard $identityCard, IdentityCardRepository $identityCardRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$identityCard->getId(), $request->request->get('_token'))) {
            $identityCardRepository->remove($identityCard, true);
        }

        return $this->redirectToRoute('app_identity_card_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/pdf/generator', name: 'app_identity_card_pdf_generator', methods: ['GET'])]
    public function IdentityCardPdf(IdentityCard $identityCard): Response
    {
        // return $this->render('pdf_generator/index.html.twig', [
        //     'controller_name' => 'PdfGeneratorController',
        // ]);
        $data = [
            'identityCardNumber' => $identityCard->getIdentityCardNumber(),
            'firstName' => $identityCard->getFirstName(),
            'lastName' => $identityCard->getLastName(),
            'birthDate' => $identityCard->getBirthDate(),
            'fatherName' => $identityCard->getFatherName(),
            'motherName' => $identityCard->getMotherName(),
            'identityCreationDate' => $identityCard->getIdentityCreationDate(),
            'address' => $identityCard->getAddress()
        ];
        $html =  $this->renderView('identity_card/pdf_generator.html.twig', $data);
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->render();
         
        return new Response (
            $dompdf->stream('resume', ["Attachment" => false]),
            Response::HTTP_OK,
            ['Content-Type' => 'application/pdf']
        );
    }
}
