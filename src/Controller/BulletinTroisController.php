<?php

namespace App\Controller;

use Dompdf\Dompdf;
use App\Entity\BulletinTrois;
use App\Form\BulletinTroisType;
use App\Repository\BulletinTroisRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/bulletinTrois')]
class BulletinTroisController extends AbstractController
{
    #[Route('/', name: 'app_bulletin_trois_index', methods: ['GET'])]
    public function index(BulletinTroisRepository $bulletinTroisRepository): Response
    {
        return $this->render('bulletin_trois/index.html.twig', [
            'bulletin_trois' => $bulletinTroisRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_bulletin_trois_new', methods: ['GET', 'POST'])]
    public function new(Request $request, BulletinTroisRepository $bulletinTroisRepository): Response
    {
        $bulletinTrois = new BulletinTrois();
        $form = $this->createForm(BulletinTroisType::class, $bulletinTrois);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bulletinTroisRepository->save($bulletinTrois, true);

            return $this->redirectToRoute('app_bulletin_trois_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('bulletin_trois/new.html.twig', [
            'bulletin_troi' => $bulletinTrois,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_bulletin_trois_show', methods: ['GET'])]
    public function show(BulletinTrois $bulletinTrois): Response
    {
        return $this->render('bulletin_trois/show.html.twig', [
            'bulletin_trois' => $bulletinTrois,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_bulletin_trois_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, BulletinTrois $bulletinTrois, BulletinTroisRepository $bulletinTroisRepository): Response
    {
        $form = $this->createForm(BulletinTroisType::class, $bulletinTrois);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bulletinTroisRepository->save($bulletinTrois, true);

            return $this->redirectToRoute('app_bulletin_trois_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('bulletin_trois/edit.html.twig', [
            'bulletin_trois' => $bulletinTrois,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_bulletin_trois_delete', methods: ['POST'])]
    public function delete(Request $request, BulletinTrois $bulletinTrois, BulletinTroisRepository $bulletinTroisRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$bulletinTrois->getId(), $request->request->get('_token'))) {
            $bulletinTroisRepository->remove($bulletinTrois, true);
        }

        return $this->redirectToRoute('app_bulletin_trois_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/pdf/generator', name: 'app_bulletin_trois_pdf_generator', methods: ['GET'])]
    public function BulletinTroisPdf(BulletinTrois $bulletinTrois): Response
    {
        // return $this->render('pdf_generator/index.html.twig', [
        //     'controller_name' => 'PdfGeneratorController',
        // ]);
        $data = [
            'firstName' => $bulletinTrois->getFirstName(),
            'lastName' => $bulletinTrois->getLastName(),
            'information' => $bulletinTrois->getInformation(),
            'dateCreation' => $bulletinTrois->getDateCreation(),
            'identity' => $bulletinTrois->getIdentityCard()->getIdentityCardNumber(),
        ];
        $html =  $this->renderView('bulletin_trois/pdf_generator.html.twig', $data);
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->render();
         
        return new Response (
            $dompdf->stream('resume', ["Attachment" => false]),
            Response::HTTP_OK,
            ['Content-Type' => 'application/pdf']
        );
    }
}
