<?php

namespace App\Entity;

use App\Repository\IdentityCardRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;

#[ORM\Entity(repositoryClass: IdentityCardRepository::class)]
#[ApiResource]
class IdentityCard
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $identityCardNumber = null;

    #[ORM\Column(length: 255)]
    private ?string $firstName = null;

    #[ORM\Column(length: 255)]
    private ?string $lastName = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $birthDate = null;

    #[ORM\Column(length: 255)]
    private ?string $fatherName = null;

    #[ORM\Column(length: 255)]
    private ?string $motherName = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $identityCreationDate = null;

    #[ORM\Column(length: 255)]
    private ?string $address = null;

    #[ORM\OneToMany(mappedBy: 'identityCard', targetEntity: BulletinTrois::class)]
    private Collection $bulletinTrois;

    public function __construct()
    {
        $this->bulletinTrois = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentityCardNumber(): ?string
    {
        return $this->identityCardNumber;
    }

    public function setIdentityCardNumber(string $identityCardNumber): self
    {
        $this->identityCardNumber = $identityCardNumber;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getFatherName(): ?string
    {
        return $this->fatherName;
    }

    public function setFatherName(string $fatherName): self
    {
        $this->fatherName = $fatherName;

        return $this;
    }

    public function getMotherName(): ?string
    {
        return $this->motherName;
    }

    public function setMotherName(string $motherName): self
    {
        $this->motherName = $motherName;

        return $this;
    }

    public function getIdentityCreationDate(): ?\DateTimeInterface
    {
        return $this->identityCreationDate;
    }

    public function setIdentityCreationDate(\DateTimeInterface $identityCreationDate): self
    {
        $this->identityCreationDate = $identityCreationDate;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<int, BulletinTrois>
     */
    public function getBulletinTrois(): Collection
    {
        return $this->bulletinTrois;
    }

    public function addBulletinTrois(BulletinTrois $bulletinTrois): self
    {
        if (!$this->bulletinTrois->contains($bulletinTrois)) {
            $this->bulletinTrois->add($bulletinTrois);
            $bulletinTrois->setIdentityCard($this);
        }

        return $this;
    }

    public function removeBulletinTrois(BulletinTrois $bulletinTrois): self
    {
        if ($this->bulletinTrois->removeElement($bulletinTrois)) {
            // set the owning side to null (unless already changed)
            if ($bulletinTrois->getIdentityCard() === $this) {
                $bulletinTrois->setIdentityCard(null);
            }
        }

        return $this;
    }
}
