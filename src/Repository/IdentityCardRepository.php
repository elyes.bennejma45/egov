<?php

namespace App\Repository;

use App\Entity\IdentityCard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<IdentityCard>
 *
 * @method IdentityCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method IdentityCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method IdentityCard[]    findAll()
 * @method IdentityCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IdentityCardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IdentityCard::class);
    }

    public function save(IdentityCard $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(IdentityCard $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

   /**
    * @return IdentityCard[] Returns an array of IdentityCard objects
    */
   public function findIdentityById($id): ?array
   {
    $query = $this->createQueryBuilder('i')
        ->select('i')
        ->where('i.id = :id')
        ->setParameter('id', $id)
        ->getQuery()
        ->getResult();

    return $query;
   }

//    public function findOneBySomeField($value): ?IdentityCard
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
